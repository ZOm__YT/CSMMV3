package fr.zom.csmmv3.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.state.IBlockState;
import net.minecraftforge.common.ToolType;

import javax.annotation.Nullable;

public class CustomBlock extends Block{

    private int harvestLevel;
    private ToolType toolType;

    public CustomBlock(String name, Block.Properties properties, float hardness, float resistance, SoundType soundType, int harvestLevel, ToolType toolType) {
        super(properties.hardnessAndResistance(hardness, resistance).sound(soundType));

        setRegistryName(name);

        this.harvestLevel = harvestLevel;
        this.toolType = toolType;

    }

    @Override
    public int getHarvestLevel(IBlockState state) {
        return harvestLevel;
    }

    @Nullable
    @Override
    public ToolType getHarvestTool(IBlockState state) {
        return toolType;
    }
}
