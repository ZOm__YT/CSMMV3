package fr.zom.csmmv3.items;

import fr.zom.csmmv3.CSMMV3;
import net.minecraft.item.Item;

public class CustomItem extends Item
{
	public CustomItem(String name, Properties properties)
	{
		super(properties.group(CSMMV3.MODGROUP));
		setRegistryName(name);
	}

	public CustomItem(String name)
	{
		super(new Properties().group(CSMMV3.MODGROUP));
		setRegistryName(name);
	}
}
