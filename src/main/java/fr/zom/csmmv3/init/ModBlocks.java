package fr.zom.csmmv3.init;

import fr.zom.csmmv3.CSMMV3;
import fr.zom.csmmv3.blocks.CustomBlock;
import fr.zom.csmmv3.utils.Refs;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.common.ToolType;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.ObjectHolder;

@Mod.EventBusSubscriber(modid = Refs.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class ModBlocks {

    @ObjectHolder(Refs.MODID + ":amethyst_block")
    public static final Block AMETHYST_BLOCK = null;

    @SubscribeEvent
    public static void registerBlocks(RegistryEvent.Register<Block> e)
    {
        e.getRegistry().register(new CustomBlock("amethyst_block",Block.Properties.create(Material.IRON), 5.0F, 20.0F, SoundType.METAL, 2, ToolType.PICKAXE));

    }

    @SubscribeEvent
    public static void registerItemBlocks(RegistryEvent.Register<Item> e)
    {
        e.getRegistry().register(new ItemBlock(AMETHYST_BLOCK, new Item.Properties().group(CSMMV3.MODGROUP)).setRegistryName(AMETHYST_BLOCK.getRegistryName()));
    }


}
