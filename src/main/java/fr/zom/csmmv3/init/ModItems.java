package fr.zom.csmmv3.init;

import fr.zom.csmmv3.CSMMV3;
import fr.zom.csmmv3.items.CustomItem;
import fr.zom.csmmv3.items.CustomPickaxe;
import fr.zom.csmmv3.tiers.CustomItemTiers;
import fr.zom.csmmv3.utils.Refs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemPickaxe;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.ObjectHolder;

@Mod.EventBusSubscriber(modid = Refs.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class ModItems
{

	@ObjectHolder(Refs.MODID + ":light_apple")
	public static final Item light_apple = null;

	@ObjectHolder(Refs.MODID + ":dark_apple")
	public static final Item dark_apple = null;

	@ObjectHolder(Refs.MODID + ":amethyst_pickaxe")
	public static final ItemPickaxe amethyst_pickaxe = null;

	@SubscribeEvent
	public static void registerItems(RegistryEvent.Register<Item> e)
	{

		e.getRegistry().register(new Item(new Item.Properties()).setRegistryName("light_apple"));

		e.getRegistry().register(new CustomItem("dark_apple"));

		e.getRegistry().register(new CustomPickaxe("amethyst_pickaxe", CustomItemTiers.AMETHYST, 1, -0.8F, new Item.Properties().group(CSMMV3.MODGROUP)));


	}

}
