package fr.zom.csmmv3;

import fr.zom.csmmv3.init.ModItems;
import fr.zom.csmmv3.utils.Refs;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(Refs.MODID)
public class CSMMV3
{
	public static final ItemGroup MODGROUP = new ItemGroup("csmmv3_group") {
		@Override
		public ItemStack createIcon() {
			return new ItemStack(ModItems.dark_apple);
		}
	};
	private static final Logger LOGGER = LogManager.getLogger();

	public CSMMV3()
	{

		FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
		FMLJavaModLoadingContext.get().getModEventBus().addListener(this::doClientStuff);

	}

	private void setup(final FMLCommonSetupEvent event)
	{

	}

	private void doClientStuff(final FMLClientSetupEvent event)
	{
	}

}
